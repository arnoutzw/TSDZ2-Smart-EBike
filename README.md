Forked from: https://github.com/OpenSource-EBike-firmware/TSDZ2_wiki/wiki 
Credits to casainho

This ebike motor controller firmware project is to be used with the Tongsheng TSDZ2 mid drive motor.


**IMPORTANT NOTES**
* Installing this firmware will void your warranty of the TSDZ2 mid drive and KT-LCD3.
* We are not responsible for any personal injuries or accidents caused by use of this firmware.
* There is no guarantee of safety using this firmware, please use it at your own risk.
* We advise you to consult the laws of your country and tailor the motor configuration accordingly.
* Please be aware of your surroundings and maintain a safe riding style..
